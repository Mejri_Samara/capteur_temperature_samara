#include <DHT.h>
#include <DHT_U.h>
#include <Servo.h>
#define DHTPIN 10
#define DHTTYPE DHT11
/* Créer un objet Servo pour contrôler le servomoteur */
Servo monServomoteur;
//créer l'objet"sensor" de type DHT:
DHT sensor(DHTPIN, DHTTYPE);

int pos = 0;
int lastpos = 0;

void setup() {
  monServomoteur.attach(9);
  Serial.begin(9600);//initiating serial com
  Serial.println(F("Servo Thermometer startup."));
  sensor.begin();//let's start the sensor
  // Attache le servomoteur à la broche D9

}

void loop() {
  delay(200);//waiting 0.2 seconds
  float t = sensor.readTemperature(); // reading temperature
  //sending temperature to serial port:
  // Make adjustments to compensate for the pointer position
  // This will move at the rate of 4.5 per degree temp. IOW, 9 will increase the temp (move dial 9 degrees of angulation or 2 degrees temperature, clockwise)
  //int angletempoffset = 3;

  Serial.print("T:");
  Serial.println(t);

  //t = constrain(t, 11, 39);
  ////  Higher angular degrees fed to the servo are counter-clockwise. IOW, setting 180 is all the way left on a dial.
  ////  This logic reverses the direction and allocates 6.4 degrees of rotation per degree C°. T
  pos = (40 - t) * 6.4;
  // To adjust the dial position
  //  pos =  pos - angletempoffset;
  //hpos = hpos + anglehumoffset;
  // Don't bother with the servo unless there's something to do
  //  if (pos != lastpos) {
  //    monServomoteur.write(pos);
  //    lastpos = pos;
  //  }

  // Fait bouger le bras de 0° à 180°
  //
  int  position = map(t, 40, 10, 180, 0);
  monServomoteur.write(position);
  //monServomoteur.write(pos);
  //  delay(15);
  //lastpos=pos;
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  if (t <= 25) {
    digitalWrite(3, HIGH);
    digitalWrite(2, LOW);
  }
  if (t > 25) {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
  }



  //  // Fait bouger le bras de 180° à 10°
  //  for (int position = 180; position >= 0; position--) {
  //    monServomoteur.write(position);
  //    delay(15);
  //  }
}
